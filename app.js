const express = require('express')
const http = require('http')
const path = require('path')
const bodyParser = require('body-parser')
var urlExists = require('url-exists');

let app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var pictureRouter = require("./routes/picture")

app.set('appName', 'Campus Academy')
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/img', pictureRouter)

app.route('/').get(function(req,res) {
    fs.readFile('./views/add.pug',function(err,data) {
        res.writeHead(200,{'Content_type':'text/html'});
        res.write(data);
        res.end();
    });
});

app.post('/formData', function(req, res) {

  var idPic = req.body.idPic
  var name = req.body.name
  var urlPic = req.body.url
  var desc = req.body.desc

  urlExists(urlPic, function(err, exists) {
    if(exists){
      const MongoClient = require('mongodb').MongoClient
      const assert = require('assert')
      const ObjectId = require('mongodb').ObjectId
      const { strictEqual } = require('assert')
      // Connection URL
      const url = 'mongodb://localhost:27017'
      // Database Name
      const dbName = 'test'
      // Create a new MongoClient
      const client = new MongoClient(url, {useNewUrlParser: true})
      // Use connect method to connect to the Server
      client.connect(function(err) {
          assert.equal(null, err)

          const db = client.db(dbName)
          colec = db.collection('pictures')

          if (req.body.cmd == "add") {

            colec.save({"name":name, "url":urlPic, "desc":desc})
            .then(function() {
              client.close()
              res.redirect('/')
            })

          } else if (req.body.cmd == "update"){
            console.log(name);
            colec.updateOne(
              {_id : ObjectId(idPic)},{$set: {"name":name,"desc":desc}}
             )
             .then(function() {
               client.close()
               res.redirect('/img/update/'+idPic)
             })
          }

      })
    } else {
      res.redirect('/img/add')
    }
  });
});


/* GET home page. */
app.get('/', function(req, res, next) {
  res.render('index')
});

http .createServer(app)
    .listen(
        app.get('port'),
        () => {
            console.log(`Express.js server ecoutes sur le port ${app.
            get('port')}`)
        } );


/*******************************************************************************
FONCTION MONGO-lo
*******************************************************************************/
