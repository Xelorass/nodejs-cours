var express = require('express');
var router = express.Router();

router.get('/show/:pic', function(req, res, next) {

  var pic = 0
  const MongoClient = require('mongodb').MongoClient;
  const assert = require('assert')
  const ObjectId = require('mongodb').ObjectId;

  const { strictEqual } = require('assert')
  // Connection URL
  const url = 'mongodb://localhost:27017'
  // Database Name
  const dbName = 'test'
  // Create a new MongoClient
  const client = new MongoClient(url, {useNewUrlParser: true})
  // Use connect method to connect to the Server
  client.connect(function(err) {
      assert.equal(null, err)
      const db = client.db(dbName)

      colec = db.collection('pictures')

      pic = colec.findOne({_id: ObjectId(req.params.pic)})
      .then(function(pic) {
        client.close()
        res.render('show', {
          picture: pic
        })
      })
  })
})

router.get('/update/:pic', function(req, res, next) {

  var pic = 0
  const MongoClient = require('mongodb').MongoClient;
  const assert = require('assert')
  const ObjectId = require('mongodb').ObjectId;

  const { strictEqual } = require('assert')
  // Connection URL
  const url = 'mongodb://localhost:27017'
  // Database Name
  const dbName = 'test'
  // Create a new MongoClient
  const client = new MongoClient(url, {useNewUrlParser: true})
  // Use connect method to connect to the Server
  client.connect(function(err) {
      assert.equal(null, err)
      const db = client.db(dbName)

      colec = db.collection('pictures')
      pic = colec.findOne({_id: ObjectId(req.params.pic)})
      .then(function(pic) {
        client.close()
        console.log(pic)
        res.render('update', {
          picture: pic
        })
      })
  })
})

router.get('/add', function(req, res, next) {
  res.render('add')
});

router.get('/delete/:pic', function(req, res, next) {

  const MongoClient = require('mongodb').MongoClient;
  const assert = require('assert')
  const ObjectId = require('mongodb').ObjectId;
  const { strictEqual } = require('assert')
  // Connection URL
  const url = 'mongodb://localhost:27017'
  // Database Name
  const dbName = 'test'
  // Create a new MongoClient
  const client = new MongoClient(url, {useNewUrlParser: true})
  // Use connect method to connect to the Server
  client.connect(function(err) {
      assert.equal(null, err)

      const db = client.db(dbName)
      colec = db.collection('pictures')
      colec.deleteOne( {"_id": ObjectId(req.params.pic)})
      .then(function() {
        client.close()
        res.redirect('/')
      })
  })
});

module.exports = router;
