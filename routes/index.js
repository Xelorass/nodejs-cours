var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  var gallerie = []

  const MongoClient = require('mongodb').MongoClient;
  const assert = require('assert')
  const { strictEqual } = require('assert')
  // Connection URL
  const url = 'mongodb://localhost:27017'
  // Database Name
  const dbName = 'test'
  // Create a new MongoClient
  const client = new MongoClient(url, {useNewUrlParser: true})
  // Use connect method to connect to the Server
  client.connect(function(err) {
      assert.equal(null, err)
      console.log("Connected successfully to server")
      const db = client.db(dbName)

      collection = db.collection('pictures')
      // Peform a simple find and return all the documents
      collection.find().forEach((picture) => {
        gallerie.push(picture)

      }).then(function(pic) {

        client.close()
        res.render('index', {
          title: 'Gallerie de photos',
          pictures: gallerie
        })
      })
  })
});

module.exports = router;
